# O que é Deadlock?

Um deadlock ocorre quando duas ou mais threads ficam aguardando a liberação do mesmo recurso.
Isso ocorre da seguinte maneira, uma thread (_thread1_) trava (lock) um recurso1, e outra thread (_thread2_) trava um trava o recurso2 e
quando a _thread1_ tenta acessar o recurso2 e se encontra travado pela _thread2_, ficando aguardando sua liberação, 
enquanto a _thread2_ tenta acessar o recurso1 fica aguardando indefinidamente a liberação da _thread1_.

Por exemplo:

```java
public static void main(String[] args) {
    final Object obj1 = new Object();
    final Object obj2 = new Object();
    
    ExecutorService executor = Executors.newFixedThreadPool(2);
    executor.submit(() -> {
        // thread 1 trava o obj1 e depois tenta travar obj2
        synchronized (obj1) {
            System.out.println("Thread 1: travando obj1");
    
            // simulando algum processamento
            try { Thread.sleep(100);} catch (Exception e) {}
    
            synchronized (obj2) {
                System.out.println("Thread 1: trvando obj2");
            }
        }
    });
    
    executor.submit(() -> {
        // thread 2 trava primeiro o obj2 e depois tenta travar obj1
        synchronized (obj2) {
            System.out.println("Thread 2: travando obj2");
    
            // simulando algum processamento
            try { Thread.sleep(100);} catch (Exception e) {}
    
            synchronized (obj1) {
                System.out.println("Thread 2: trvando obj1");
            }
        }
    });
    
    executor.shutdown();
}
```

E terá o seguinte output:

```
Thread 1: travando obj1
Thread 2: travando obj2
```

A execução nunca termina.

---

Exemplo 2:
```java
static class Client {

  private String name;
    private BigDecimal wallet;

    public Client (String name, BigDecimal wallet) {
        this.name = name;
        this.wallet = wallet;
    }

    public synchronized void transferTo(Client client, BigDecimal value) {
        System.out.println(String.format("%s - Transferindo %s para %s.", this.getName(), value, client.getName()));
        this.wallet = this.wallet.add(value);
        System.out.println(String.format("%s - Saldo de %s.", this.getName(), getWallet()));
        System.out.println(String.format("%s - Enviando dinheiro...", this.getName()));
        client.receive(value);
    }

    public synchronized void receive(BigDecimal value) {
        System.out.println(String.format("%s - Dinheiro recebido! Adicionando a conta.", this.getName()));
        this.wallet = this.wallet.subtract(value);
        System.out.println(String.format("%s - Novo valor da carteira %s.", this.getName(), this.getWallet()));
    }

    public String getName() {
        return name;
    }

    public BigDecimal getWallet() {
        return wallet;
    }
}

public static void main(String[] args) {
    final Client client1 = new Client("Joao", new BigDecimal("100.00"));
    final Client client2 = new Client("Maria", new BigDecimal("100.00"));

    ExecutorService executor = Executors.newFixedThreadPool(2);
    executor.submit(() -> {
        client1.transferTo(client2, new BigDecimal("10.00"));
    });

    executor.submit(() -> {
        client2.transferTo(client1, new BigDecimal("15.00"));
    });

    executor.shutdown();
}

```

Output:
```
Maria - Transferindo 15.00 para Joao.
Joao - Transferindo 10.00 para Maria.
Joao - Saldo de 110.00.
Maria - Saldo de 115.00.
Joao - Enviando dinheiro...
Maria - Enviando dinheiro...
```

Ninguém nunca recebe um centavo.