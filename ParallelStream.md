# ParallelStreams

No java8 podemos utilizar 2 tipos de _stream_, o _stream_ comum que é síncrono
e o _prallelStream_ que como o nome já diz é paralelo ou assíncrono.

O _parallelStream_ não pode ser utilizado em qualquer tipo de operação, 
como quando a ordem do cálculo não irá interferir em seu resultado, 
e devemos ter a atenção redobrada quando se está utilizando objetos compartilhados.

# Como funciona o _parallel_

Quando se é invocado collection._prallelStream_() ou collection._stream_()._prallel_()
a stream é divida em diversas partes para serem processados independentemente por cada 
um dos núcleos existentes na CPU, ou pela definição propriedade 
`java.util.concurrent.ForkJoinPool.common.parallelism` que irá definir a quantidade 
de threads a serem utilizadas, e no final são sumarizadas.

Exemplo 1:

Caso o algoritmo seja uma função   

```java
Stream<Long> numbers = LongStream.range(1, 10000).boxed();
System.out.println(numbers.reduce((a,b) -> a + b));

numbers = LongStream.range(1, 10000).boxed().parallel();
System.out.println(numbers.reduce((a,b) -> a + b));
```

Output
```
Optional[49995000]
Optional[49995000]
```

Exemplo 2:
```java
Stream<Long> numbers = LongStream.range(1, 10000).boxed();
System.out.println(numbers.reduce((a,b) -> (long)((Math.sqrt(a) * 2) + Math.sqrt(b)) ));

numbers = LongStream.range(1, 10000).boxed().parallel();
System.out.println(numbers.reduce((a,b) -> (long)((Math.sqrt(a) * 2) + Math.sqrt(b))));
```

Output*
```
Optional[121]
Optional[10]
```

Exemplo 3:
```java
Stream<Long> numbers = LongStream.range(1, 10000).boxed();
System.out.println(numbers.reduce((a,b) -> (long)((Math.sqrt(a) * 2) * Math.sqrt(b)) ));

numbers = LongStream.range(1, 10000).boxed().parallel();
System.out.println(numbers.reduce((a,b) -> (long)((Math.sqrt(a) * 2) * Math.sqrt(b))));
```

Output*
```
Optional[39990]
Optional[271776]
```

\* Resultados podem varia de acordo com número de núcleos do computador 
ou da config "java.util.concurrent.ForkJoinPool.common.parallelism".

Exemplo 4:
```java
System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", "4");

Stream<Long> numbers = LongStream.range(1, 10000).boxed();
System.out.println(numbers.reduce((a,b) -> (long)((Math.sqrt(a) * 2) * Math.sqrt(b)) ));

numbers = LongStream.range(1, 10000).boxed().parallel();
System.out.println(numbers.reduce((a,b) -> (long)((Math.sqrt(a) * 2) * Math.sqrt(b))));
```

Output*
```
Optional[39990]
Optional[500145]
```

Um bom ponto para ser observado é que caso se tenha um algoritmo que tem alta utilização da CPU, 
poderá ocupar todo o tempo de processamento da aplicação, não deixando
espaço para outros tarefas serem executadas.

 