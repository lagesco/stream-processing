package com.dk.streamprocessing.util;

import com.dk.streamprocessing.core.Stream;

import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

/**
 * Classe utilitária para busca do primeiro char
 *
 * @author Caio Andrade (dk)
 */
public class Util {
    private static final String VOWEL_LIST = "aeiouAEIOU";

    private Util() {
        super();
    }

    /**
     * Retorna a primeira vogal não repetida após uma consoante seguida de uma vogal
     *
     * @param input stream a ser processada
     * @return Optional contendo o caracter ou e Optinal.empty caso não encontre o caracter
     */
    public static Optional<Character> firstChar(Stream input) {
        Set<Character> vowelFound = new TreeSet<>();
        char[] chars = new char[3];
        byte index = 0;

        // evitado recursividade pois poderá ocorrer java.lang.StackOverflowError
        while (input.hasNext()) {
            chars[index++] = input.getNext();

            if (index < 3) {
                continue;
            }
            char first = chars[0];
            char second = chars[1];
            char third = chars[2];

            boolean v1 = isVowel(first), v2 = isVowel(second), v3 = isVowel(third);

            // adiciona a lista de vogais existentes
            if (v1) {
                vowelFound.add(Character.toLowerCase(first));
            }

            boolean allLetters =
                    (v1 || isLetter(first))
                            && (v2 || isLetter(second))
                            && (v3 || isLetter(third));

            // caso first = vogal, second = consoante e thid = vogal não existente anteriormente (vowelFound)
            if (allLetters && v1 && !v2 && v3 && !vowelFound.contains(Character.toLowerCase(third))) {
                return Optional.of(third);
            }

            // shift back one position
            {
                index = 2;
                chars[0] = chars[1];
                chars[1] = chars[2];
                chars[2] = 0;
            }
        }

        return Optional.empty();
    }

    /**
     * Verifica se o char informado é uma letra
     *
     * @param c char a ser verificado
     * @return true - caso seja uma letra
     */
    public static boolean isLetter(char c) {
        return Character.isLetter(c);
    }

    /**
     * Verifica se o char informado é uma vogal
     *
     * @param c char a ser verificado
     * @return true - caso seja uma vogal
     */
    public static boolean isVowel(char c) {
        return VOWEL_LIST.indexOf(c) > -1;
    }
}
