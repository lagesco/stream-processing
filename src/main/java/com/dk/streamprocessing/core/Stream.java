package com.dk.streamprocessing.core;

/**
 * Interface stream para String / char[]
 *
 * @author Caio Andrade (dk)
 */
public interface Stream {

    /**
     * Obtém próximo caracter da stream
     *
     * @return próximo char da stream
     */
    char getNext();

    /**
     * Verifica se ainda contem caracters
     *
     * @return true - se contém caracteres a serem processados
     */
    boolean hasNext();

}
