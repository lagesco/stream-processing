package com.dk.streamprocessing.core.impl;

import com.dk.streamprocessing.core.Stream;
import com.dk.streamprocessing.core.exception.StreamEndedException;

/**
 * Implementação stream para String/char[]
 *
 * @author Caio Andrade (dk)
 */
public class StreamImpl implements Stream {
    private char[] input;
    private int index = 0;

    public StreamImpl(String input) {
        this(input.toCharArray());
    }

    public StreamImpl(char[] input) {
        this.input = input;
    }

    @Override
    public char getNext() {
        try {
            return input[index++];
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new StreamEndedException();
        }
    }

    @Override
    public boolean hasNext() {
        return index + 1 <= input.length;
    }

}
