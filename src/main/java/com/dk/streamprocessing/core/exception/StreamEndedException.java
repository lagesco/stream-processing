package com.dk.streamprocessing.core.exception;

/**
 * Exception lançada quando tenta obter o próximo caracter da stream mas está já atingiu seu final
 *
 * @author Caio Andrade (dk)
 */
public class StreamEndedException  extends RuntimeException {

    public StreamEndedException() {
        super();
    }

    public StreamEndedException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public StreamEndedException(final String message) {
        super(message);
    }

    public StreamEndedException(final Throwable cause) {
        super(cause);
    }
}

