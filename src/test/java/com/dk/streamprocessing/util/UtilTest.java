package com.dk.streamprocessing.util;

import com.dk.streamprocessing.core.Stream;
import com.dk.streamprocessing.core.exception.StreamEndedException;
import com.dk.streamprocessing.core.impl.StreamImpl;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Classe utilitária para busca do primeiro char
 *
 * @author Caio Andrade (dk)
 */
public class UtilTest {

    @Test(expected = StreamEndedException.class)
    public void testThrowException() {
        Stream stream = new StreamImpl("ab");
        for (int i = 0; i < 3; i++) {
            stream.getNext();
        }
    }

    @Test
    public void testEmptyString() {
        Stream stream = new StreamImpl("");
        Assertions.assertThat(Util.firstChar(stream))
                .as("Não existe caracter não vogal não repetido")
                .isEmpty();
    }

    @Test
    public void test1() {
        char expectedOutput = 'e';

        Stream stream = new StreamImpl("aAbBABacafe");
        assertThat(Util.firstChar(stream).get())
                .as("O primeiro caracter não repetido ver ser a vogal '" + expectedOutput + "'")
                .isEqualTo(expectedOutput);
    }

    @Test
    public void test2() {
        char expectedOutput = 'o';

        Stream stream = new StreamImpl("aAbBABo");
        assertThat(Util.firstChar(stream).get())
                .as("O primeiro caracter não repetido ver ser a vogal '" + expectedOutput + "'")
                .isEqualTo(expectedOutput);
    }

    @Test
    public void testEmpty() {
        Stream stream = new StreamImpl("aBa");
        assertThat(Util.firstChar(stream))
                .as("Não existe caracter não vogal não repetido")
                .isEmpty();
    }

    @Test
    public void test4() {
        char expectedOutput = 'i';

        Stream stream = new StreamImpl("aBi");
        assertThat(Util.firstChar(stream).get())
                .as("O primeiro caracter não repetido ver ser a vogal '" + expectedOutput + "'")
                .isEqualTo(expectedOutput);
    }

    @Test
    public void test5() {
        char expectedOutput = 'i';

        Stream stream = new StreamImpl("aBbaaBio");
        assertThat(Util.firstChar(stream).get())
                .as("O primeiro caracter não repetido ver ser a vogal '" + expectedOutput + "'")
                .isEqualTo(expectedOutput);
    }

    @Test
    public void test6() {
        char expectedOutput = 'i';

        Stream stream = new StreamImpl("aBbaaBioBe");
        assertThat(Util.firstChar(stream).get())
                .as("O primeiro caracter não repetido ver ser a vogal '" + expectedOutput + "'")
                .isEqualTo(expectedOutput);
    }

    @Test
    public void test7() {
        char expectedOutput = 'u';
        Stream stream = new StreamImpl("aBbaiBuoBe");

        assertThat(Util.firstChar(stream).get())
                .as("O primeiro caracter não repetido ver ser a vogal '" + expectedOutput + "'")
                .isEqualTo(expectedOutput);
    }

    @Test
    public void testFile() throws IOException, URISyntaxException {
        char expectedOutput = 'o';
        Path path = Paths.get(getClass().getClassLoader().getResource("file-test.txt").toURI());

        StringBuilder data = new StringBuilder();
        java.util.stream.Stream<String> lines = Files.lines(path);
        lines.forEach(line -> data.append(line));
        lines.close();

        Stream stream = new StreamImpl(data.toString().toCharArray());
        assertThat(Util.firstChar(stream).get())
            .as("O primeiro caracter não repetido ver ser a vogal '" + expectedOutput + "'")
            .isEqualTo(expectedOutput);
    }

    @Test
    public void testNumberEmpty() {
        Stream stream = new StreamImpl("aBa1i");
        assertThat(Util.firstChar(stream))
            .as("Não existe caracter não vogal não repetido")
            .isEmpty();
    }

    @Test
    public void testNumber() {
        char expectedOutput = 'o';

        Stream stream = new StreamImpl("aBa1ixo");
        assertThat(Util.firstChar(stream).get())
                .as("O primeiro caracter não repetido ver ser a vogal '" + expectedOutput + "'")
                .isEqualTo(expectedOutput);
    }

}
