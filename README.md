# Sobre o projeto
Dada uma stream, encontre o primeiro caractere Vogal, após uma consoante,
onde a mesma é antecessora a uma vogal e que não se repita no resto da stream. O
termino da leitura da stream deve ser garantido através do método hasNext(), ou
seja, retorna falso para o termino da leitura da stream. Voce tera acesso a leitura da
stream através dos métodos de interface fornecidos ao termino do enunciado.

## Premissas:

- Uma chamada para hasNext() ir retornar se a stream ainda contem caracteres para processar.
- Uma chamada para getNext() ir retornar o proximo caractere a ser processado na stream.
- Não será possível reiniciar o fluxo da leitura da stream.
- Não poderá ser utilizado nenhum framework Java, apenas código nativo.

Exemplo:

```
Input: aAbBABacafe

Output: e
```

 No exemplo, ‘e’ é o primeiro caractere Vogal da stream que não se repete após a primeira
Consoante ‘f’o qual tem uma vogal ‘a’ como antecessora.
Segue o exemplo da interface em Java:

```java
public interface Stream {
    
    public char getNext();
    
    public boolean hasNext();
    
}

public static char firstChar(Stream input) {
    ...
}
```

## Resolução

Foi criado uma interface [Stream.java](/src/main/java/com/dk/streamprocessing/core/Stream.java)
e uma implemtação para esta [StreamImpl.java](/src/main/java/com/dk/streamprocessing/core/impl/StreamImpl.java), 
que pode receber uma `String` como `char[]` como parametro.
O algoritmo que realiza a busca da primeira caracter vogal (Definição em _Sobre o projeto_), encontra-se na classe 
[Util.java](/src/main/java/com/dk/streamprocessing/util/Util.java) utilizando o método **firstChar**.

Osb.: Só foi utilizado framework para a realização dos teste.

### Testes

Os teste podem ser encontrados em [UtilTest.java](/src/test/java/com/dk/streamprocessing/util/UtilTest.java), 
o qual realiza teste em 11 cenários.

#### Executando os teste
Para executar os teste, abra o terminal, navegue até o diretório da aplicação e execute o comando abaixo:

```cmd
mvn test
```

# Questões de Deadlock e parallelStreams

As repostas a estas quetões podem ser encontradas em: [Deadlock.md](Deadlock.md) e [ParallelStream.md](ParallelStream.md).


